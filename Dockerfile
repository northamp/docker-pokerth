FROM debian:11.6-slim as build

RUN apt update && apt install -y pokerth-server

COPY pokerth-docker-entrypoint.sh /opt/

RUN ls -A1 /etc/skel | xargs rm -rf && useradd -m -s /bin/bash pokerth && \
  mkdir /srv/pokerth && chown pokerth: /srv/pokerth && ln -s /srv/pokerth /home/pokerth/.pokerth && \
  chmod +x /opt/pokerth-docker-entrypoint.sh

USER pokerth

WORKDIR /srv/pokerth

ENTRYPOINT ["/opt/pokerth-docker-entrypoint.sh"]