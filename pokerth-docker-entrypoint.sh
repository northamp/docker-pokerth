#!/bin/bash

if [ $# -eq 0 ]
  then
    echo Starting PokerTH server
    /usr/games/pokerth_server
    trap "kill `cat ~/.pokerth/log-files/pokerth.pid`; exit" TERM EXIT
    tail -f ~/.pokerth/log-files/server_messages.log
else
  $@
fi
