# Dockerized PokerTH

Simple Docker image able to run a PokerTH server.

## Quickstart

```bash
docker run -d -v ${PWD}:/srv/pokerth -p 7234:7234 registry.gitlab.com/northamp/docker-pokerth/pokerth:latest
```

## Configuration & data

Server will store its data in `/srv/pokerth`. On first run, it will generate the configuration and directory structure required by the game.

It can be persisted by using a host mount as depicted in the quick-start.
